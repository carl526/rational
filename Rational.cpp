#include <iostream>
using namespace std;

class Rational {
	public:
		Rational();
		Rational(int a, int b);
		Rational(const Rational& R);
		const Rational& operator=(const Rational& R);
		~Rational();
		Rational operator+(const Rational& R) const;
		Rational operator-(const Rational& R) const;
		Rational operator*(const Rational& R) const;
		Rational operator/(const Rational& R) const;
		Rational operator++();
		Rational operator++(int a);
		bool operator==(const Rational& R) const;
		bool operator>(const Rational& R) const;
		bool operator<(const Rational& R) const;
		operator int() const;
		operator double() const;
		int getNum() const;
		int getDen() const;
		int getID() const;
		int getCount() const;

	private:
		int numerator;
		int denominator;
		int objectID;
		static int objectCount;
		int gcf(int a, int b) const;
		int lcm(int a, int b) const;
		friend istream& operator>>(istream& i, Rational& r);
		friend ostream& operator<<(ostream& o, const Rational& r);
};

Rational::Rational(int a, int b) {
	numerator = a;
	denominator = b;
}

Rational::Rational() {
	numerator = 0;
	denominator = 1;
}

Rational::Rational(const Rational& R) {
	numerator = R.getNum();
	denominator = R.getDen();
}

Rational::~Rational() {}

Rational Rational::operator+(const Rational& R) const {
	int n1 = numerator;
	int d1 = denominator;
	int n2 = R.getNum();
	int d2 = R.getDen();
	int d = gcf(d1, d2);
	n1 = (d/d1)*n1;
	n2 = (d/d2)*n2;

	return Rational(n1+n2,d);
}

Rational Rational::operator-(const Rational& R) const {
	int n1 = numerator;
	int d1 = denominator;
	int n2 = R.getNum();
	int d2 = R.getDen();
	int d = gcf(d1, d2);
	n1 = (d/d1)*n1;
	n2 = (d/d2)*n2;

	return Rational(n1-n2,d);
}

Rational Rational::operator*(const Rational& R) const {
	return Rational(numerator*R.getNum(),denominator*R.getDen());
}

Rational Rational::operator/(const Rational& R) const {
	return Rational(numerator*R.getDen(), denominator*R.getNum());
}

Rational Rational::operator++() {
	denominator = gcf(denominator,1);
	return Rational(numerator+denominator,denominator);
}

Rational Rational::operator++(int) {
	Rational temp = Rational(numerator, denominator);
	denominator = gcf(denominator,1);
	numerator = numerator+denominator;
	return temp;
}

bool Rational::operator==(const Rational& R) const {
	if ((numerator*R.getDen()) == (denominator*R.getNum())) return 1;
	return 0;
}

bool Rational::operator>(const Rational& R) const {
	if ((numerator*R.getDen()) > (denominator*R.getNum())) return 1;
	return 0;
}

bool Rational::operator<(const Rational& R) const {
	if ((numerator*R.getDen()) < (denominator*R.getNum())) return 1;
	return 0;
}

Rational::operator double() const {
	return double(double(numerator)/denominator);
}

int Rational::getNum() const {
	return numerator;
}

int Rational::getDen() const {
	return denominator;
}

int Rational::gcf(int a, int b) const {
	if (a == 0 || b	 == 0)
		return 0;
	if (a == b)
		return a;
	if (a > b)
		return gcf(a-b, b);
	return gcf(a, b-a);
}

int Rational::lcm(int a, int b) const {
	return (a*b)/gcf(a,b);
}


